# Please edit PREFIX and INSTALL_DIR to your needs.

BUNDLE = Sampler-example.lv2
PREFIX = /usr
INSTALL_DIR = $(PREFIX)/lib/lv2

CFLAGS = -Wall -ggdb


$(BUNDLE): manifest.ttl sampler.ttl sampler.so monosample.wav
	rm -rf $(BUNDLE)
	mkdir $(BUNDLE)
	cp $^ $(BUNDLE)

sampler.so: sampler.c
	gcc $(CFLAGS) -shared -fPIC  sampler.c `pkg-config --cflags --libs lv2core` `pkg-config --cflags --libs sndfile` -lpthread -o sampler.so

install: $(BUNDLE)
	mkdir -p $(INSTALL_DIR)
	rm -rf $(INSTALL_DIR)/$(BUNDLE)
	cp -R $(BUNDLE) $(INSTALL_DIR)

clean:
	rm -rf $(BUNDLE) sampler.so
