/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil -*-
 *
 * This program is public domain.  See the end for a list of
 * authors.
 *
 */

/* sampler.c
 *
 * A simple example of an LV2 sampler that dynamically loads samples
 * (based on incoming events) and also triggers their playback (based
 * on incoming MIDI note events).  The sample must be monophonic.
 *
 * So that the runSampler() method stays real-time safe, the plugin
 * creates a worker thread (worker_thread_main) that listens for file
 * loading events.  It loads everything in plugin->pending_samp and
 * then signals the runSampler() that it's time to install it.
 * runSampler() just has to swap pointers... so the change happens
 * very fast and atomically.
 */

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <pthread.h>
#include <assert.h>

#include <sndfile.h>
#include <lv2.h>

#include "lv2_event-helpers.h"
#include "lv2_uri-map.h"

#define SAMPLER_URI     "http://lv2plug.in/plugins/example_sampler";
#define MIDI_EVENT_URI  "http://lv2plug.in/ns/ext/midi#MidiEvent"
#define STRING_BUF 8192

enum {
    SAMPLER_OUTPUT_PORT = 0,
    SAMPLER_MIDI_IN = 1
};

static const char* default_sample_file = "monosample.wav";

static LV2_Descriptor *samplerDescriptor = NULL;

typedef struct _Sampler Sampler;
typedef struct _SampleFile SampleFile;

struct _SampleFile
{
    char     filepath[STRING_BUF];
    SF_INFO  info;
    float*   data;
};

struct _Sampler
{
    /* the sample */
    SampleFile*     samp;
    SampleFile*     pending_samp;
    int             pending_sample_ready;
    pthread_mutex_t pending_samp_mutex; /* protects pending_samp */
    pthread_cond_t  pending_samp_cond;  /* signaling mechanism */

    /* ports */
    float*              outputPort;
    LV2_Event_Buffer*   eventPort;
    LV2_Event_Feature*  event_ref;
    int midi_event_id;

    /* playback */
    bool        play;
    sf_count_t  frame;

    /* new file loading */
    pthread_t       worker_thread;
};


static void handle_load_sample(Sampler* plugin)
{
    SNDFILE *sample;
    SF_INFO *info;
    float *data;

    plugin->pending_sample_ready = 0;

    info = &plugin->pending_samp->info;
    sample = sf_open(plugin->pending_samp->filepath,
                     SFM_READ,
                     info);

    if(!sample
       || !info->frames
       || info->channels != 1) /* mono */
    {
        fprintf(stderr, "failed to open sample '%s'.\n",
                plugin->pending_samp->filepath);
        return;
    }

    /* read data */
    data = malloc(sizeof(float) * info->frames);
    plugin->pending_samp->data = data;

    if(!data)
    {
        fprintf(stderr, "failed to allocate memory for sample.\n");
        return;
    }

    sf_seek(sample, 0ul, SEEK_SET);
    sf_read_float(sample,
                  data,
                  info->frames);
    sf_close(sample);

    /* 
     * Queue the sample for installation on next runSample()
     */
    plugin->pending_sample_ready = 1;
}

void* worker_thread_main(void* arg)
{
    Sampler *plugin;

    plugin = (Sampler*) arg;

    pthread_mutex_lock(&plugin->pending_samp_mutex);
    for(;;) {
        /* Wait for runSampler() to signal that we need to
         * load a sample, and then load it.
         */
        pthread_cond_wait(&plugin->pending_samp_cond,
                          &plugin->pending_samp_mutex);
        handle_load_sample(plugin);
    }

    return 0;
}

static void cleanupSampler(LV2_Handle instance)
{
    Sampler* plugin = (Sampler*)instance;
    pthread_cancel(plugin->worker_thread);
    pthread_join(plugin->worker_thread, 0);

    free(plugin->samp->data);
    free(plugin->pending_samp->data);
    free(plugin->samp);
    free(plugin->pending_samp);
    free(instance);
}


static void connectPortSampler(LV2_Handle instance, uint32_t port,
                                                    void *data)
{
    Sampler* plugin = (Sampler*)instance;

    switch(port)
    {
    case SAMPLER_OUTPUT_PORT:
        plugin->outputPort = (float*)data;
        break;
    case SAMPLER_MIDI_IN:
        plugin->eventPort = (LV2_Event_Buffer*)data;
        break;
    default:
        break;
    }
}


static LV2_Handle instantiateSampler(const LV2_Descriptor *descriptor,
                                     double s_rate, const char *path,
                                     const LV2_Feature* const* features)
{
    int i;

    Sampler* plugin = (Sampler *)malloc(sizeof(Sampler));
    assert(plugin);
    memset(plugin, 0, sizeof(Sampler));

    plugin->samp = (SampleFile*) malloc(sizeof(SampleFile));
    assert(plugin->samp);
    memset(plugin->samp, 0, sizeof(SampleFile));
    plugin->pending_samp = (SampleFile*) malloc(sizeof(SampleFile));
    assert(plugin->pending_samp);
    memset(plugin->pending_samp, 0, sizeof(SampleFile));

    plugin->midi_event_id = -1;
    plugin->event_ref = 0;

    /*
     * Initialize mutexes and conditions for the worker thread
     */
    if(pthread_mutex_init(&plugin->pending_samp_mutex, 0))
    {
        fprintf(stderr, "Could not initialize next_sample_mutex.\n");
        goto fail;
    }
    if(pthread_cond_init(&plugin->pending_samp_cond, 0))
    {
        fprintf(stderr, "Could not initialize next_sample_waitcond.\n");
        goto fail;
    }
    if(pthread_create(&plugin->worker_thread, 0, worker_thread_main, plugin))
    {
        fprintf(stderr, "Could not initialize worker thread.\n");
        goto fail;
    }

    /*
     * scan host features for event and uri-map
     */
    for (i = 0; features[i]; ++i)
    {
        if (strcmp(features[i]->URI, LV2_URI_MAP_URI) == 0)
        {
            LV2_URI_Map_Feature*
                map_feature = (LV2_URI_Map_Feature *)features[i]->data;

            plugin->midi_event_id =
                map_feature->uri_to_id(map_feature->callback_data,
                                       LV2_EVENT_URI,  MIDI_EVENT_URI );
        }
        else if (strcmp(features[i]->URI, LV2_EVENT_URI) == 0)
        {
            plugin->event_ref = (LV2_Event_Feature *)features[i]->data;
        }
    }

    if (plugin->midi_event_id == -1)
    {
        /* host does not support event and uri-map extensions */
        fprintf(stderr, "host does not support event and/or uri-map"
                        "LV2 extensions.\n");
        goto fail;
    }

    /*
     * Open the default sample file
     */
    strncpy(plugin->pending_samp->filepath, path, STRING_BUF);
    strncat(plugin->pending_samp->filepath,
            default_sample_file,
            STRING_BUF - strlen(plugin->pending_samp->filepath) );
    handle_load_sample(plugin);

    return (LV2_Handle)plugin;

fail:
    free(plugin);
    return 0;
}


static void runSampler(LV2_Handle instance, uint32_t sample_count)
{
    Sampler* plugin = (Sampler*)instance;
    LV2_Event* ev = NULL;
    LV2_Event_Iterator iterator;

    sf_count_t start_frame = 0;
    sf_count_t pos = 0;
    float* output = plugin->outputPort;

    /*
     * Iterate through incoming events for misc. messages
     */
    lv2_event_begin(&iterator, plugin->eventPort);
    while (lv2_event_is_valid(&iterator))
    {
        ev = lv2_event_get(&iterator, NULL);

        if (ev->type == 0)
        {
            if (plugin->event_ref)
            {
                plugin->event_ref->lv2_event_unref(
                            plugin->event_ref->callback_data, ev);
            }
        }
        else if (ev->type == plugin->midi_event_id)
        {
            uint8_t* data = (uint8_t*)(ev + 1);

            if ((data[0] & 0xf0) == 0x90)
            {
                start_frame = ev->frames;
                plugin->frame = 0;
                plugin->play = true;
            }
        }
        /***************************************************
         * XXX TODO:                                       *
         * ADD CODE HERE TO DETECT AN INCOMING MESSAGE TO  *
         * DYNAMICALLY LOAD A SAMPLE                       *
         ***************************************************
         */
        else if (0) /* message to load a sample comes in */
        {
            /* write filename to plugin->pending_samp->filepath */
            /* strncpy(plugin->pending_samp->filepath, some_src_string, STRING_BUF); */
            pthread_cond_signal(&plugin->pending_samp_cond);
        }

        lv2_event_increment(&iterator);
    }

    /*
     * Render the sample (possibly already in progress)
     */
    if(plugin->play)
    {

        uint32_t f = plugin->frame;
        uint32_t lf = plugin->samp->info.frames;

        for (pos = 0; pos < start_frame; ++pos)
            output[pos] = 0;

        for (; pos < sample_count && f < lf; ++pos, ++f)
            output[pos] = plugin->samp->data[f];

        plugin->frame = f;

        if (f == lf)
            plugin->play = false;
    }

    /*
     * Check if we have a sample pending
     */
    if(!plugin->play
       && plugin->pending_sample_ready
       && pthread_mutex_trylock(&plugin->pending_samp_mutex))
    {
        /* Install the new sample */
        SampleFile *tmp;
        tmp = plugin->samp;
        plugin->samp = plugin->pending_samp;
        plugin->pending_samp = tmp;
        plugin->pending_sample_ready = 0;
        free(plugin->pending_samp->data);

        pthread_mutex_unlock(&plugin->pending_samp_mutex);
    }

    /*
     * Add zeros to end if sample not long enough (or not playing)
     */
    for (; pos < sample_count; ++pos)
        output[pos] = 0;
}

static void init()
{
    samplerDescriptor = (LV2_Descriptor *)malloc(sizeof(LV2_Descriptor));

    samplerDescriptor->URI =            SAMPLER_URI;
    samplerDescriptor->activate =       NULL;
    samplerDescriptor->cleanup =        cleanupSampler;
    samplerDescriptor->connect_port =   connectPortSampler;
    samplerDescriptor->deactivate =     NULL;
    samplerDescriptor->instantiate =    instantiateSampler;
    samplerDescriptor->run =            runSampler;
    samplerDescriptor->extension_data = NULL;
}


LV2_SYMBOL_EXPORT
const LV2_Descriptor *lv2_descriptor(uint32_t index)
{
    if (!samplerDescriptor)
        init();

    if (index)
        return NULL;

    return samplerDescriptor;
}

/* AUTHORS:
 * Whoever made the LV2 amp-example
 * James Morris <jwm.art.net@gmail.com>
 * Gabriel M. Beddingfield <gabriel@teuton.org>
 */
